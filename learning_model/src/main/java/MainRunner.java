import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.graph.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.schedule.MapSchedule;
import org.nd4j.linalg.schedule.ScheduleType;

public class MainRunner {

    static String[] users = { "eversaur", "Troggie42", "MadDany94", "Jenks44", "Stargov1", "farox", "Sariel007",
            "wastingtoomuchthyme", "AdorableCartoonist", "queensage77", "Joelblaze", "Riaayo", "unnecessary_kindness" };
    static ComputationGraph[] userSpecificNNs = new ComputationGraph[users.length];
    static MultiLayerNetwork[] userSpecificCNNs = new MultiLayerNetwork[users.length];
    static int EPOCHS = 50;

    public static void main(String[] args) {

        try {
            BufferedReader bufReader = new BufferedReader(new FileReader(new File("comment_data_features_norm.csv")));
            ArrayList<String> lines = new ArrayList<String>();
            // Get headers from CSV file
            String[] Headers = bufReader.readLine().split(",");
            String nextLine;

            while ((nextLine = bufReader.readLine()) != null) {
                lines.add(nextLine);
            }
            bufReader.close();
            ArrayList<double[]> allCommentData = new ArrayList<double[]>();
            int lineIndex = 0;

            for (String line : lines) {
                String[] split = line.split(",");
                // Data value by index
                // ===================
                // Word_Count,Avg_Word_Len,Punctuation_Count,Newline_Count,Adjective_Count,Noun_Count,Pronoun_Count,Adverb_Count,Adverb_Comparitive_Count,Adverb_Superlative_Count,Symbol_Count,Interjection_Count,Verb_Count,IsUser

                double[] commentData = new double[split.length];

                for (int i = 0; i < split.length - 1; i++) {
                    commentData[i] = Double.parseDouble(split[i]);
                 
                    if (i == split.length - 2) {
                        // Replace username with integer as classifier so that it can be encoded
                        commentData[i + 1] = (double) (Arrays.asList(users).indexOf(split[split.length - 1]));
                    }

                }
                allCommentData.add(commentData);
                lineIndex++;
            }

            try {
                int index = 0;
                
                for (String user : users) {
                    userSpecificNNs[index] = ComputationGraph.load(new File("./nns/" + users[index] + "_trained.zip"),
                            false);
                    userSpecificCNNs[index] = MultiLayerNetwork
                            .load(new File("./nns/" + users[index] + "_CNN_trained.zip"), false);
                    index++;
                }

                System.out.println("Found pre-existing NNs, would you like to use them? (y/n)");
                Scanner scan = new Scanner(System.in);
                String answer = scan.nextLine();
                System.out.println(answer.toLowerCase());

                if (!answer.toLowerCase().equals("y")) {
                    System.out.println("Ok, retraining models.");
                    throw new IOException();
                }

            } catch (IOException e) {
                System.out.println("Training models.");
                generateAndTrainNetworks(allCommentData);
            }

            testTrainedNetworks(allCommentData);

        } catch (IOException e) {
            System.out.println("Could not read data file:");
            System.out.println(e);
        }

    }

    // TODO: Implement testing for CNNS

    public static void testTrainedNetworks(ArrayList<double[]> comment_data) {
        for (String user : users) {

            int curUserEncoding = Arrays.asList(users).indexOf(user);

            ArrayList<Double> isUserVals = new ArrayList<Double>();
            ArrayList<Double> isNotUserVals = new ArrayList<Double>();
            ArrayList<Double> isUserValsCNN = new ArrayList<Double>();
            ArrayList<Double> isNotUserValsCNN = new ArrayList<Double>();

            int currentUserTests = 0;
            int nonCurrentUserTests = 0;
            int currentUserTestsCNN = 0;
            int nonCurrentUserTestsCNN = 0;

            double midval = 0;
            double midvalCNN = 0;

            for (double[] comment : comment_data) {

                INDArray[] INPs = new INDArray[1];

                INDArray inputArray = Nd4j.zeros(new int[] { 1, comment.length - 1 });

                for (int x = 0; x < comment.length - 1; x++) {
                    inputArray.putScalar(0, x, comment[x]);
                }

                INPs[0] = inputArray;

                boolean isCurrentUser = ((double) comment[comment.length - 1] == (double) curUserEncoding) ? true
                        : false;

                INDArray[] result = userSpecificNNs[curUserEncoding].output(INPs);
                INDArray CNNResult = userSpecificCNNs[curUserEncoding].output(inputArray);

                // System.out.println("RESULT" );
                // System.out.println(result[0]);
                // System.out.println("EXPECTED");
                // System.out.println((isCurrentUser) ? 1:0);

                if (isCurrentUser) {
                    isUserVals.add(result[0].getDouble(0));
                    isUserValsCNN.add(CNNResult.getDouble(0));
                    currentUserTests++;
                    currentUserTestsCNN++;
                } else {
                    isNotUserVals.add(result[0].getDouble(0));
                    isNotUserValsCNN.add(CNNResult.getDouble(0));
                    nonCurrentUserTests++;
                    nonCurrentUserTestsCNN++;
                }
                if (currentUserTests == nonCurrentUserTests) {
                    break;
                }

            }

            // Get RNN midval

            double sumIsUser = 0;
            for (double val : isUserVals) {
                sumIsUser += val;
            }

            double sumIsNotUser = 0;
            for (double val : isNotUserVals) {
                sumIsNotUser += val;
            }

            midval = Math.abs((sumIsUser / isUserVals.size()) - (sumIsNotUser / isNotUserVals.size()))
                    + Math.min((sumIsUser / isUserVals.size()), (sumIsNotUser / isNotUserVals.size()));

            // Get CNN midval

            double sumIsUserCNN = 0;
            for (double val : isUserValsCNN) {
                sumIsUserCNN += val;
            }

            double sumIsNotUserCNN = 0;
            for (double val : isNotUserValsCNN) {
                sumIsNotUserCNN += val;
            }

            midvalCNN = Math.abs((sumIsUserCNN / isUserValsCNN.size()) - (sumIsNotUserCNN / isNotUserValsCNN.size()))
                    + Math.min((sumIsUserCNN / isUserValsCNN.size()), (sumIsNotUserCNN / isNotUserValsCNN.size()));

            int correctClassifications = 0;
            int falseAcceptances = 0;
            int falseRejections = 0;
            int correctClassificationsCNN = 0;
            int falseAcceptancesCNN = 0;
            int falseRejectionsCNN = 0;

            int commentIndex = 0;
            for (double[] comment : comment_data) {

                if (commentIndex > currentUserTests * 2) {
                    break;
                }
                INDArray[] INPs = new INDArray[1];

                INDArray inputArray = Nd4j.zeros(new int[] { 1, comment.length - 1 });

                for (int x = 0; x < comment.length - 1; x++) {
                    inputArray.putScalar(0, x, comment[x]);
                }

                INPs[0] = inputArray;

                boolean isCurrentUser = ((double) comment[comment.length - 1] == (double) curUserEncoding) ? true
                        : false;

                INDArray[] result = userSpecificNNs[curUserEncoding].output(INPs);
                INDArray CNNResult = userSpecificCNNs[curUserEncoding].output(INPs[0]);

                // Results for RNN

                if ((isCurrentUser && (result[0].getDouble(0) > midval))
                        || (!isCurrentUser && (result[0].getDouble(0) < midval)))
                    correctClassifications++;

                if (isCurrentUser && (result[0].getDouble(0) < midval)) {
                    falseRejections++;
                }
                if (!isCurrentUser && (result[0].getDouble(0) > midval)) {
                    falseAcceptances++;
                }

                // Results for CNN

                if ((isCurrentUser && (CNNResult.getDouble(0) > midvalCNN))
                        || (!isCurrentUser && (CNNResult.getDouble(0) < midvalCNN)))
                    correctClassificationsCNN++;

                if (isCurrentUser && (CNNResult.getDouble(0) < midvalCNN)) {
                    falseRejectionsCNN++;
                }
                if (!isCurrentUser && (CNNResult.getDouble(0) > midvalCNN)) {
                    falseAcceptancesCNN++;
                }

                commentIndex++;
            }

            // System.out.println("Is user average val = " + (sumIsUser / isUserVals.size())
            // + " and is not user average val = " + (sumIsNotUser / isNotUserVals.size()));
            System.out.println("----------------------------------------\n" + user
                    + "\n===========================================");
            System.out.println("RNN =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
            System.out.println("Correctly classified " + correctClassifications + " / " + (currentUserTests * 2) + "("
                    + String.format("%.3f", ((double) correctClassifications / ((double) currentUserTests * 2)) * 100)
                    + "%)");
            System.out.println("False Rejection Rate = " + ((double) falseRejections / (currentUserTests * 2))
                    + " False Acceptance Rate = " + ((double) falseAcceptances / (currentUserTests * 2)));
            System.out.println("CNN =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
            System.out.println(
                    "Correctly classified " + correctClassificationsCNN + " / " + (currentUserTestsCNN * 2) + "("
                            + String.format("%.3f",
                                    ((double) correctClassificationsCNN / ((double) currentUserTestsCNN * 2)) * 100)
                            + "%)");
            System.out.println("False Rejection Rate = " + ((double) falseRejectionsCNN / (currentUserTestsCNN * 2))
                    + " False Acceptance Rate = " + ((double) falseAcceptancesCNN / (currentUserTestsCNN * 2)));

        }

    }

    public static void generateAndTrainNetworks(ArrayList<double[]> comment_data) {
        // TODO: For each epoch, use an equal split of current user and random other
        // users comments
        Date date = new Date();
        Long timeStart = date.getTime();
        // Each user will have their own CNN
        for (String user : users) {

            System.out.println("Training for user " + user + "\n=====================================");

            int curUserEncoding = Arrays.asList(users).indexOf(user);

            // Pass the length of input data incase this changes later
            userSpecificNNs[curUserEncoding] = BuildNetwork(comment_data.get(0).length - 1);
            userSpecificCNNs[curUserEncoding] = BuildCNN(comment_data.get(0).length - 1);

            for (int i = 0; i < EPOCHS; i++) {

                int currentUserCommentCount = 0;
                int nonCurrentUserCommentCount = 0;

                ArrayList<INDArray[]> trainingDataIn = new ArrayList<INDArray[]>();
                ArrayList<INDArray[]> trainingDataOut = new ArrayList<INDArray[]>();

                for (double[] comment : comment_data) {

                    // Since user data is bunched in the dataset, once we finished parsing all of a
                    // users comments we are able to exit the loop.
                    boolean foundUser = false;

                    if ((double) curUserEncoding == comment[comment.length - 1]) {
                        // System.out.println("Current user encoding = " + (double)curUserEncoding + "
                        // and comment val = " + comment[comment.length-1] + " matches = " +
                        // currentUserCommentCount);
                        // This is the current users comment, train on it

                        foundUser = true;

                        INDArray[] INPs = new INDArray[1];
                        INDArray[] OUTs = new INDArray[1];

                        INDArray inputArray = Nd4j.zeros(new int[] { 1, comment.length - 1 });
                        INDArray outputArray = Nd4j.zeros(new int[] { 1, 1 });

                        for (int x = 0; x < comment.length - 1; x++) {
                            inputArray.putScalar(0, x, comment[x]);
                        }

                        outputArray.putScalar(0, 0, 1);

                        INPs[0] = inputArray;
                        OUTs[0] = outputArray;

                        // Add data to training set
                        trainingDataIn.add(INPs);
                        trainingDataOut.add(OUTs);

                        currentUserCommentCount++;

                    } else if (foundUser) {
                        break;
                    }
                }

                Random rand = new Random();
                for (int z = 0; z < currentUserCommentCount; z++) {

                    int testCommentIndex = rand.nextInt(comment_data.size());
                    double[] comment = comment_data.get(testCommentIndex);

                    if ((double) curUserEncoding == comment[comment.length - 1]) {
                        z--;
                        continue;
                    }

                    if (!((double) curUserEncoding == comment[comment.length - 1])
                            && (nonCurrentUserCommentCount < currentUserCommentCount)) {
                        // There is less non user comments than user comments, add one to the training
                        // data

                        INDArray[] INPs = new INDArray[1];
                        INDArray[] OUTs = new INDArray[1];

                        INDArray inputArray = Nd4j.zeros(new int[] { 1, comment.length - 1 });
                        INDArray outputArray = Nd4j.zeros(new int[] { 1, 1 });

                        for (int x = 0; x < comment.length - 1; x++) {
                            inputArray.putScalar(0, x, comment[x]);
                        }

                        outputArray.putScalar(0, 0, 0);

                        INPs[0] = inputArray;
                        OUTs[0] = outputArray;

                        // Add data to training set
                        trainingDataIn.add(INPs);
                        trainingDataOut.add(OUTs);

                        nonCurrentUserCommentCount++;
                    }
                    // Break if we have the same or greater amount of non-user comments
                    else if ((nonCurrentUserCommentCount >= currentUserCommentCount)) {
                        break;
                    }

                }

                // Since the data is bunched by username, lets shuffle it first
                // done by creating a shuffled list of indices to keep trainingDataIn
                // and trainingDataOut in sync

                ArrayList<Integer> shuffleArr = new ArrayList<Integer>();
                for (int f = 0; f < trainingDataIn.size(); f++) {
                    shuffleArr.add(f);
                }

                Collections.shuffle(shuffleArr);
                int shufLength = shuffleArr.size();

                for (int f = 0; f < shufLength; f++) {
                    // System.out.println(trainingDataOut.size());
                    // System.out.println(trainingIndex);
                    // System.out.println(trainingDataIn.get(0)[0]);

                    userSpecificNNs[curUserEncoding].fit(trainingDataIn.get(f), trainingDataOut.get(f));
                    userSpecificCNNs[curUserEncoding].fit(trainingDataIn.get(f)[0], trainingDataOut.get(f)[0]);
                    System.out.print("RNN Accuracy = " + String.format("%.4f", userSpecificNNs[curUserEncoding].score())
                            + ", CNN Accuracy = " + String.format("%.4f", userSpecificCNNs[curUserEncoding].score())
                            + " after " + i + " epochs. " + String.format("%.4f", ((double) f / shufLength) * 100)
                            + "% done current epoch \r");

                }

            }
            date = new Date();
            Long endTime = date.getTime();
            System.out.println(timeStart);
            System.out.println(endTime);
            System.out.println(endTime - timeStart);
            System.out.println("Trained " + (curUserEncoding + 1) + "/" + users.length + " users " + " in "
                    + ((((double) endTime - timeStart) / 1000) / 60) + " minutes.");

        }
        int index = 0;
        for (ComputationGraph nn : userSpecificNNs) {
            try {
                nn.save(new File("./nns/" + users[index] + "_trained.zip"));
                index++;
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        index = 0;
        for (MultiLayerNetwork nn : userSpecificCNNs) {
            try {
                nn.save(new File("./nns/" + users[index] + "_CNN_trained.zip"));
                index++;
            } catch (IOException e) {
                System.out.println(e);
            }
        }

    }

    public static MultiLayerNetwork BuildCNN(int inputDataSize) {
        Map<Integer, Double> learningRateSchedule = new HashMap<>();

        learningRateSchedule.put(0, 0.08);
        learningRateSchedule.put(EPOCHS / 5, 0.05);
        learningRateSchedule.put(EPOCHS / 4, 0.028);
        learningRateSchedule.put(EPOCHS / 3, 0.006);
        learningRateSchedule.put(EPOCHS / 2, 0.001);

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(1234).l2(0.0005) // Ridge regression
                                                                                                  // value TODO: Google
                                                                                                  // this
                .updater(new Nesterovs(new MapSchedule(ScheduleType.ITERATION, learningRateSchedule)))
                .weightInit(WeightInit.XAVIER).list()
                .layer(new ConvolutionLayer.Builder(1, 1).nIn(inputDataSize).stride(1, 1).nOut(20)
                        .activation(Activation.IDENTITY).build())
                .layer(new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX).kernelSize(1, 1).stride(2, 2)
                        .build())
                .layer(new ConvolutionLayer.Builder(1, 1).stride(1, 1).nOut(50).activation(Activation.IDENTITY).build())
                .layer(new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(1, 1).stride(2, 2).build())
                .layer(new DenseLayer.Builder().activation(Activation.RELU).nOut(500).build())
                .layer(new OutputLayer.Builder(LossFunctions.LossFunction.XENT).nOut(1) // Binary Classifier
                        .activation(Activation.SIGMOID).build())
                .setInputType(InputType.convolutionalFlat(1, 1, inputDataSize)).build();

        MultiLayerNetwork net = new MultiLayerNetwork(conf);

        return net;
    }

    public static ComputationGraph BuildNetwork(int inputDataSize) {
        // TODO: Implement a NN to learn comment tendencies
        // Some hyperparameters (variable) declarations here.
        double learningRate = 0.00001; // Learning rate

        ComputationGraphConfiguration config = new NeuralNetConfiguration.Builder().weightInit(WeightInit.XAVIER)
                .activation(Activation.ELU).updater(new Adam(learningRate))

                .graphBuilder()

                /* Begin. Start creating the neural network structure (Layers) here */

                .addInputs("vector_in")

                .addLayer("INPUT_I1", new DenseLayer.Builder().nIn(inputDataSize).nOut(30).build(), "vector_in")

                .addLayer("HIDDEN_H1", new DenseLayer.Builder().nIn(30).nOut(30).build(), "INPUT_I1")

                .addLayer("HIDDEN_H2", new DenseLayer.Builder().nIn(30).nOut(30).build(), "HIDDEN_H1")

                .addLayer("HIDDEN_H3", new DenseLayer.Builder().nIn(30).nOut(30).build(), "HIDDEN_H2")

                .addLayer("HIDDEN_H4", new DenseLayer.Builder().nIn(30).nOut(30).build(), "HIDDEN_H3")

                .addLayer("HIDDEN_H5", new DenseLayer.Builder().nIn(30).nOut(5).build(), "HIDDEN_H4")

                .addLayer("OUTPUT_O1",
                        new OutputLayer.Builder(LossFunctions.LossFunction.XENT).activation(Activation.SIGMOID).nIn(5)
                                .nOut(1).build(),
                        "HIDDEN_H5")

                .setOutputs("OUTPUT_O1").build();

        ComputationGraph net = new ComputationGraph(config);
        net.init();

        return net;
    }

}
