# Reddit Author Recognition

Author: Kyle Behiels (kylejamesbehiels@gmail.com)

## Description

The goal of this project was to determine the viability of identifying authors of reddit comments based on the comment alone. Assumptions being that authors have a certain "signature" way of writing. In this project we extract the meaningful data that make up these "signatures" and test the performance of two separate machine learning algorithms in the context of author identification.  

## Results

### CNN Performance

While the results were mixed, in the majority of cases (8/13) the CNN outperformed the RNN.

![model_accuracy](./media/model_accuracy.png)

The CNN's averaged an accuracy of 81.3%.

### RNN Performance

The RNN's averaged an accuracy of 80.0%

### Aggregate

Both models show good performance in accuracy, as well as false acceptance and rejection rates.

![all_data](./media/all_data.png)

Interestingly accuracy seemed to be unaffected by the size of the training data.

![training_data_size](./media/model_accuracy_relative_to_data_size.png)

For more information you can take a look at the results in our paper [here.](./media/Author_Recognition_for_Reddit_Comments_Using_Machine_Learning.pdf)

## Running the code

### Reddit Scraper

1. Install dependencies `pip install praw nltk`
2. Run gatherData.py first `python3 gatherData.py`
3. Run feature_extraction.py `python3 feature_extraction.py`

### Java ML Models

NOTE: This project should theoretically work on all systems though it has only been tested on linux

1. Ensure the GPU drivers are up to date on your system. For NVIDIA on linux this meant installing the cuda specific drivers
    - This step is optional but GPU accelaration makes a big difference in training times
2. Install a JRE (Version 11 or greater)
3. Compile `MainRunner.java` and run

If you download the NN zip files in `/learning_model/nns/` they can be used instead of training a new batch.