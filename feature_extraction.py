import csv, nltk, string

min_word_count = 10

# NLTK Tags for words
Adjective = ['JJ', 'JJR','JJS']
Noun = ['NN', 'NNP', 'NNPS', 'NNS']
Pronoun = ['PRP', 'PRP$']
Adverb = ['RB']
Adverb_Comparative = ['RBR']
Adverb_Superlative = ['RBS']
Symbol = ['SYM']
Interjection = ['UH']
Verb = ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']

usernames = {"eversaur", "Troggie42", "MadDany94", "Jenks44", "Stargov1", "farox", "Sariel007", "wastingtoomuchthyme", "AdorableCartoonist", "queensage77", "Joelblaze", "Riaayo", "unnecessary_kindness"  }


def main():

    read_file = open("reddit_comments.csv", "r")
    csvReader = csv.reader(read_file)

    fields = next(csvReader)

    count = lambda l1,l2: sum([1 for x in l1 if x in l2])


    data = []
    for line in csvReader:
        data.append(line)

    read_file.close()

    write_file = open("comment_data_features.csv", "w")
    csvWriter = csv.writer(write_file)


    writeable_parsed =[["Word_Count", "Avg_Word_Len", "Punctuation_Count", "Newline_Count", "Adjective_Count", "Noun_Count", "Pronoun_Count", "Adverb_Count", "Adverb_Comparitive_Count", "Adverb_Superlative_Count", "Symbol_Count", "Interjection_Count", "Verb_Count", "IsUser"]]

    index = 0
    for point in data:
        
        print("Parsed ", index, " of ", len(data))
        index += 1

        newLineCount = 0
        punctuationCount = 0

        for i in range(0,len(point[0])):
            if(point[0][i:i+5] == '&COM;' or point[0][i:i+7] == '&NEWLN;' or point[0][i:i+6] == '&QUOT;' or point[0][i:i+6] == '&APOS;'):
                if(point[0][i:i+7] == '&NEWLN;'):
                    newLineCount += 1
                if(point[0][i:i+5] == '&COM;' or point[0][i:i+6] == '&QUOT;' or point[0][i:i+6] == '&APOS;'):
                    punctuationCount += 1

        punctuationCount += count(point[0], set(string.punctuation))

        restored_comment = point[0].replace('&COM;', ',' ).replace("&NEWLN;", "\n").replace("&QUOT;", '\"').replace("&APOS;", "\'")
        
        words = nltk.word_tokenize(restored_comment)

        if(len(words) < min_word_count):
            continue


        wordLengthSum = 0
        for word in words:
            wordLengthSum += len(word)
        
        AvgWordLength = wordLengthSum / len(words)

        wordCount = str(len(words))

        taggedText = nltk.pos_tag(words)

        counts = {"Adjective": 0, 
                "Noun": 0, 
                "Pronoun": 0, 
                "Adverb": 0, 
                "Adverb_Comparative": 0, 
                "Adverb_Superlative": 0, 
                "Symbol": 0, 
                "Interjection": 0, 
                "Verb": 0}

        for tagged in taggedText:
            if(tagged[1] in Adjective):
                counts["Adjective"]+=1
            if(tagged[1] in Noun):
                counts["Noun"]+=1
            if(tagged[1] in Pronoun):
                counts["Pronoun"]+=1
            if(tagged[1] in Adverb):
                counts["Adverb"]+=1
            if(tagged[1] in Adverb_Comparative):
                counts["Adverb_Comparative"]+=1
            if(tagged[1] in Adverb_Superlative):
                counts["Adverb_Superlative"]+=1
            if(tagged[1] in Symbol):
                counts["Symbol"]+=1
            if(tagged[1] in Interjection):
                counts["Interjection"]+=1
            if(tagged[1] in Verb):
                counts["Verb"]+=1

        
        writeable_row = [
                        len(words),
                        AvgWordLength,
                        punctuationCount / len(words), 
                        newLineCount/ len(words), 
                        counts["Adjective"] / len(words), 
                        counts["Noun"] / len(words), 
                        counts["Pronoun"] / len(words), 
                        counts["Adverb"] / len(words), 
                        counts["Adverb_Comparative"] / len(words), 
                        counts["Adverb_Superlative"] / len(words), 
                        counts["Symbol"] / len(words), 
                        counts["Interjection"] / len(words), 
                        counts["Verb"] / len(words), 
                        point[1]
        ]

        writeable_parsed.append(writeable_row)
            
        
    csvWriter.writerows(writeable_parsed)

    write_file.close()

main()